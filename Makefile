OBJFILES=galsim.o particles.o graphics.o
CFLAGS=-I./graphics -std=gnu99
LDFLAGS=-L/opt/X11/lib -lX11 -lm

all: galsim

galsim: ${OBJFILES}
	gcc  ${OBJFILES} ${LDFLAGS} -o galsim  

graphics.o: graphics/graphics.c
	gcc -c ${CFLAGS} -I/opt/X11/include graphics/graphics.c -o graphics.o

%.o: %.c %.h
	gcc -c $< ${CFLAGS} -o $@


clean:
	rm -rf *.o galsim