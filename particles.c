#include "particles.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>


void allocate_particles(Particles *parts, int N)
{
    parts->mass = malloc(N*sizeof(double));
    parts->posX = malloc(N*sizeof(double));
    parts->posY = malloc(N*sizeof(double));
    parts->velX = malloc(N*sizeof(double));
    parts->velY = malloc(N*sizeof(double));
    parts->brightness = malloc(N*sizeof(double));
}

void free_particles(Particles *parts)
{
    free(parts->mass);
    free(parts->posX);
    free(parts->posY);
    free(parts->velX);
    free(parts->velY);
    free(parts->brightness);
}

void save_to_galaxy_file(const char *filename, Particles *parts)
{
    FILE *outfile = fopen(filename, "w");
    for(int i = 0; i < parts->N_particles; i++)
    {
        fwrite(&(parts->posX[i]), sizeof(double), 1, outfile);
        fwrite(&(parts->posY[i]), sizeof(double), 1, outfile);
        fwrite(&(parts->mass[i]), sizeof(double), 1, outfile);
        fwrite(&(parts->velX[i]), sizeof(double), 1, outfile);
        fwrite(&(parts->velY[i]), sizeof(double), 1, outfile);
        fwrite(&(parts->brightness[i]), sizeof(double), 1, outfile);
    }
    fclose(outfile);
}

void parse_galaxy_file(const char *filename, Particles *parts)
{
    FILE* infile = fopen(filename, "r");

    for(int i = 0; i < parts->N_particles; i++)
    {
        fread(&(parts->posX[i]), sizeof(double), 1, infile);
        fread(&(parts->posY[i]), sizeof(double), 1, infile);
        fread(&(parts->mass[i]), sizeof(double), 1, infile);
        fread(&(parts->velX[i]), sizeof(double), 1, infile);
        fread(&(parts->velY[i]), sizeof(double), 1, infile);
        fread(&(parts->brightness[i]), sizeof(double), 1, infile);
    }

    fclose(infile);
}

void print_particle(int i, Particles *parts)
{
    printf("Particle nr: %d\n\tPosition: (%f, %f)\n\tVelocity: (%f, %f)\n\tMass: %f\n", 
            i,
            parts->posX[i], parts->posY[i], 
            parts->velX[i], parts->velY[i], 
            parts->mass[i]);
}

void update_particle_forces(int i, Particles *parts, double dt)
{
    double G = (100.0/(double)(parts->N_particles));
    double Ax = 0;
    double Ay = 0;
    double rx, ry, rlen, tmp;


    // Calculate force contribution from every particle
    for(int j = 0; j < parts->N_particles; j++) 
    {
        rx = parts->posX[i] - parts->posX[j];
        ry = parts->posY[i] - parts->posY[j];

        rlen = (double)sqrt(rx*rx + ry*ry);

        tmp = parts->mass[j] / ((rlen + epsilon)*(rlen + epsilon)*(rlen + epsilon));
        Ax += tmp*rx;
        Ay += tmp*ry;
    }

    // Update acceleration
    Ax *= -G;
    Ay *= -G;

    // Update velocity
    parts->velX[i] += dt * Ax;
    parts->velY[i] += dt * Ay;
}


void update_particle_velocities(int i, Particles *parts, double dt)
{
    // Update position
    parts->posX[i] += dt * parts->velX[i];
    parts->posY[i] += dt * parts->velY[i];
}